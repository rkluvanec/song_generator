var fs = require("fs");
var xml2js = require("xml2js");
/**
 * This is a class that allows loading and parsing MusicXML stems  
 */
class Library {
    /**
     * Initialize the library
     * @param path The filesystem path to the music library
     */
    constructor(path){
        this.path = path;
        // Laod the configuration file
        this.config = require(this.path + "genres_config.json"); 
        // Boot a cache for the stems
        this.stems = {};
    }

    /**
     * Returns a configuration of a given genre
     * @param genre 
     */
    loadGenreConfig(genre){
        return this.config[genre];
    }

    /*
     * Helper function to save a parsed stem to the local cache
     */
    saveStemToCache(stem, instrument, genre, variant, part, minor){
        if(!minor) minor = false;
        var key = instrument + genre + variant + part + minor;
        this.stems[key] = stem;
    }

    /*
     * Helper function to load a parsed stem to the local cache
     */
    loadStemFromCache(instrument, genre, variant, part, minor){
        if(!minor) minor = false;
        var key = instrument + genre + variant + part + minor;
        return this.stems[key];
    }

    /*
     * Checks if a stem has already been parsed and cached
     */
    hasStemInCache(instrument, genre, variant, part, minor){
        if(!minor) minor = false;
        var key = instrument + genre + variant + part + minor;
        return (key in this.stems);
    }

    /**
     * Interface function to load and parse a specific MusicXML stem
     */
    getStem(instrument, genre, variant, part, minor){
        if(!minor) minor = false;
        // Check if the stem has already been parsed and is in cache
        if(this.hasStemInCache(instrument, genre, variant, part, minor)){
            // Load the stem from the cache
            return this.loadStemFromCache(instrument, genre, variant, part, minor);
        } else {
            // Load and parse the stem from the file system
            var minor_str;
            if(minor) {
                minor_str = "_minor";
            } else {
                minor_str = "";
            }
            try{
                var score = fs.readFileSync(this.path + instrument + "/" + genre + "/" + variant + "/" + part + minor_str + ".musicxml");
                xml2js.parseString(score, function(err, res){score = res});
                var stem = score["score-partwise"]["part"][0].measure;    
            } catch (err) {
                var stem = this.getEmpty();
            }
            // Save the stem to the cache
            this.saveStemToCache(stem, instrument, genre, variant, part, minor);
            return stem;
        }
    }

    getMasterFile(){
        var musicxml = fs.readFileSync(this.path + "master.xml");
        var master = [];
        xml2js.parseString(musicxml, function(err, data){
            master = data;
        });
        return master;
    }

    getEmpty(){
        var empty_str = fs.readFileSync(this.path + "empty.xml");
        var empty;
        xml2js.parseString(empty_str, function(err, data){
            empty = data;
        });

        return empty.measure;

    }
}

module.exports = Library;