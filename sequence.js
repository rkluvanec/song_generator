/**
 * This is a helper class used to track the current position of the song
 */
class Sequence{
    /**
     * Initialize a new sequence
     * @param sequence Configuration from the Music library
     */
    constructor(sequence){
        this.timeline = sequence.timeline;
        this.duration = sequence.duration;
        this.currentBar = 0;
        this.currentPart = 0;
    }

    /**
     * Returns the current Bar name and the current position
     */
    getNextBar(){
        var part = {
            name: this.timeline[this.currentPart].part,
            bar: this.currentBar
        }
        this.currentBar++;
        if(this.currentBar == this.timeline[this.currentPart].duration){
            this.currentBar = 0;
            this.currentPart++;
        }
        if(this.currentPart == this.timeline.length) this.resetPosition();
        return part;
    }

    /**
     * Resets the current position in the sequence
     */
    resetPosition(){
        this.currentBar = 0;
        this.currentPart = 0;
    }

    /**
     * Returns the total duration of the sequence
     */
    getDuration(){
        return this.duration;
    }
}

module.exports = Sequence;