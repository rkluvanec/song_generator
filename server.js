var express = require('express');
var path = require('path');
var generateSong = require("./main.js");

var app = express();

app.get('/', function (req, res) {
    if(req.query.generate == "true"){
        try{
            var song = generateSong(req.query.genre, req.query.seed, req.query.length);
        } catch(err) {
            res.send("An error has occured:\n" + err);
        }
        if(song){
            res.set({"Content-Disposition":"attachment; filename=\"" + req.query.seed + req.query.genre + req.query.length + ".musicxml\""});
            res.send(song);
        }
    } else {
        res.sendFile(path.join(__dirname + '/index.html'));
    }
});

app.listen(3000); // Webserver is running on port 3000
