var Song = require("./song.js");
var Library = require("./library.js");
var Sequence = require("./sequence.js");
var seed = 42;

/**
 * Converts provided string into a numerical hash
 * @param s The string to be hashed
 */
function hash(s){
    return s.split("").reduce(function(a,b){a=((a<<5)-a)+b.charCodeAt(0);return a&a},0);              
}

/**
 * Returns a random seeded number
 */
function random() {
    var x = Math.sin(seed++) * 10000;
    return x - Math.floor(x);
}

/**
 * Generate a MusicXML file with the provided settings
 * @param user_genre Genre of the song
 * @param user_hash String that is used to seed the randomization 
 * @param user_length Length of the song
 */
var generateSong = function(user_genre, user_hash, user_length){

    // Seed the randomization function
    var user_hash = user_hash || "roman"
    seed = parseInt(hash(user_hash));

    // Create a Song object
    var song = new Song();
    
    // Load the dependencies 
    var library = new Library("./music_library/");
    var genre = user_genre || "pop";
    var song_config = library.loadGenreConfig(genre);

    // Randomly select a chord progression for the song
    var harmony = song_config.harmonies[Math.floor(random()*song_config.harmonies.length)];
    console.log(harmony);

    // Randomly select a key for the song
    var base_key = Math.floor(random() * (12));

    // Calculate tempo & Select sequence
    var time_length = parseInt(user_length) || 120;
    var tempos = [];

    // Calculate the bitrate of each possible sequence with the given length
    for(var x in song_config.sequences){
        var beat_length = song_config.sequences[x].duration * 4;
        var seq_tempo = Math.floor(beat_length / time_length * 60);
        if(seq_tempo >= song_config.tempo.min && seq_tempo <= song_config.tempo.max) tempos.push({seq: x, tempo: seq_tempo});
        console.log(beat_length + " " + seq_tempo)
    }

    // Select a sequence that can have the desired length
    if(tempos.length == 0) throw new Error("Invalid length of the song");
    var final_tempo = tempos[Math.floor(random()*tempos.length)];
    var sequence = new Sequence(song_config.sequences[final_tempo.seq]);
    
    // Configure the song object
    var duration = sequence.getDuration();
    song.setTempo(final_tempo.tempo);
    song.setDuration(duration);

    // Generate the scores for all instruments
    if(song_config && song_config.instruments){
        for(var instrument in song_config.instruments){
            // Select an instrument
            var instrument_object = song_config.instruments[instrument];
            
            // Choose a random variant
            var variant = instrument_object.variants[Math.floor(random()*instrument_object.variants.length)];
			console.log(instrument + " " + variant);

            // Generate the score if the instrument is available in this configuration
            if(variant != "-1"){

                // Generate each bar 
                for(var i = 0; i < duration; i++){

                    // Look up the next part and the corresponding chord
                    var part = sequence.getNextBar();
                    var key = harmony[part.bar % harmony.length];
                    var seq = part.name;

                    // Load the corresponding stem from the Music library
                    var stem = [];
                    if(instrument == "drums" || key >= 0){
                        stem = library.getStem(instrument, genre, variant, seq);
                    } else {
                        stem = library.getStem(instrument, genre, variant, seq, true);
                    }

                    // Add the stem to the song
                    song.addMeasure(instrument, stem[part.bar % stem.length], Math.abs(key) + base_key % 12 - 6);
                }    
            }
        }
    }

    // Load the helper files from the library
    var master = library.getMasterFile();
    var empty = library.getEmpty();
    
    // Export the song
    try {
        var xml = song.exportXML(master, empty);
    } catch(err) {
        throw Error("Unable to export the MusicXML. Please check the soundbank.")
    }
    return xml;
}

module.exports = generateSong;
