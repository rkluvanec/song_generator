var xml2js = require("xml2js");

/**
 * Song is a class that represents a song.
 * It stores all the respective scores for each instrument and handles the MusicXML export
 */
class Song {
    constructor(){
        // List of the possible instruments
        this.instruments = {
            drums: {id: "P1", melody: []},
            piano: {id: "P2", melody: []},
            bass: {id: "P3", melody: []},
            guitar: {id: "P4", melody: []},
            saxophone: {id: "P5", melody: []},
            brass: {id: "P6", melody: []},
            flute: {id: "P7", melody: []},
            electric: {id: "P8", melody: []},
            organ: {id: "P9", melody: []},
        };
        
        // Set default length and tempo
        this.length = 1;
        this.tempo = 128;

    }

    // Enable an instrument
    addInstrument(name, header){
        var instrument = {
            name: name,
            header: header,
            melody: []
        }
        this.instruments[name] = instrument;
    }

    // Add a measure for a specific instrument and transpose it
    addMeasure(insrument, measure, key){
        this.instruments[insrument].melody.push({measure: measure, key: key});
    }

    // Set the duration of the song
    setDuration(duration){
        this.duration = duration;
    }

    // Set the tempo of the song
    setTempo(tempo){
        this.tempo = tempo;
    }

    // Export the song to MusicXML format
    exportXML(master, empty){

        // ID counter for the instrument MIDI interface
        var i = 1;

        // Export every instrument
        for(var x in this.instruments){

            // Clear the master file and assign it the current instrument
            master["score-partwise"]["part"] = Object.assign([],master["score-partwise"]["part"]) || [];
            master["score-partwise"]["part"].push({'$': {id: this.instruments[x].id}, measure: []});
            
            // Initialize the current octave change
            var octave_change = 0;

            // Iterate over every measure
            for(var y = 0; y < this.duration; y++){
                let measure = {};
                let key = "0";

                // Set tempo
                var direction = [{
                    $: {placement: "above"},
                    "direction-type": [{"words": [{}]}],
                    sound: [{$: {"tempo": this.tempo}}]
                }]
                measure.direction = Object.assign([],direction);
                measure.attributes = [];
                measure["$"] = {};

                // Load the measure
                let measure_obj = {};
                if(y < this.instruments[x].melody.length){
                    measure_obj = Object.assign({}, this.instruments[x].melody[y].measure);
                    key = this.instruments[x].melody[y].key;
                } else {
                    measure_obj = Object.assign({}, empty);
                }

                // Build the measure object in the musicXML format
                for(var m in measure_obj){
                    if(m != "direction") measure[m] = measure_obj[m];
                } 

                // Disable scaling of the leadsheet
                measure.print = {};
                measure["$"].width = "100";

                // Transpose the measure to the corresponding chord
                if(x != "drums"){
                    measure.attributes = Object.assign([],measure.attributes) || [{}];
                    if(measure.attributes.length == 0) measure.attributes.push({});
                    if(measure.attributes[0] && measure.attributes[0].transpose && measure.attributes[0].transpose[0]){
                        if(measure.attributes[0].transpose[0]["octave-change"]) octave_change = measure.attributes[0].transpose[0]["octave-change"];
                    }
                    var transpose = [{diatonic: 0, chromatic: key, "octave-change": octave_change}];
                    measure.attributes[0].transpose = Object.assign([],transpose);
                }

                // Write the measure to the output object
                master["score-partwise"]["part"][i - 1]["measure"].push(JSON.parse(JSON.stringify(measure)));
                master["score-partwise"]["part"][i - 1]["measure"][y]["$"].number = "";
            }
            i++;
        }

        // Build the MusicXML file from the output object
        var builder = new xml2js.Builder();
        return builder.buildObject(master);
    }

}

module.exports = Song;