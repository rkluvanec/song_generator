# Music generator
Music generator is a web-application that generates MusicXML files

# Installation
You have to install the required NPM packages.
Launch the server to initialize the app.
```bash
npm install
```

# Launching
Launch the server to initialize the app.
```bash
node server.js
```

# Webpage
The web-application is available on `http://localhost:3000`
